// Copyright 2018 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// WARNING: THIS FILE IS MACHINE GENERATED. DO NOT EDIT.
// Generated from the banjo.examples.syzkaller.flag banjo file

#![allow(unused_imports, non_camel_case_types)]

use fuchsia_zircon as zircon;


// C ABI compat




#[repr(u32)]
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum topic_t {
    TOPIC0 = 0,
    TOPIC1 = 1,
    TOPIC2 = 2,
    TOPIC3 = 3,
    TOPIC4 = 4,
    TOPIC5 = 5,
}



