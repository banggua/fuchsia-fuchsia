// Copyright 2019 The Fuchsia Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#pragma once
#define MT8183_MCUCFG_BASE  0x0c530000
#define MT8183_MCUCFG_SIZE  0x2000

// MCU config interrupt polarity registers start
#define MT8183_MCUCFG_INT_POL_CTL0 0xa80
